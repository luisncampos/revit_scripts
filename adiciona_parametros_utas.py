import os

doc = __revit__.ActiveUIDocument.Document

params = [
"Diametro_conexao_quente",
"Altura_conexao_quente_in",
"Altura_conexao_quente_out",
"Distancia_conexao_quente_in",
"Distancia_conexao_quente_out",
"Diametro_conexao_fria",
"Altura_conexao_fria_in",
"Altura_conexao_fria_out",
"Distancia_conexao_fria_in",
"Distancia_conexao_fria_out"]

addToGroup = BuiltInParameterGroup.PG_GEOMETRY
parameterType = ParameterType.Length

t = Transaction(doc,"Adiciona parametros das UTA's")
try:
	t.Start()

	for i,j in enumerate(params):
		j = doc.FamilyManager.AddParameter(j, addToGroup, parameterType,False)
	t.Commit()
except:
	t.ForceCloseTransaction()
