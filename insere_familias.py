from __future__ import print_function

doc = __revit__.ActiveUIDocument.Document

family_path = r"\\192.168.25.12\FileServer\02. Dep. Tecnico\0012 - Climacer\01. Hospital CUF Tejo\02.12. BIM - Families\HVAC\Pipe Ring FB.rfa"


t = Transaction(doc,"Familias inseridas no documento")

try:
	t.Start()
	doc.LoadFamily(family_path)
	t.Commit()
except Exception,e:
	print(e)
	t.RollBack()
	