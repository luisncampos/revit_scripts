from __future__ import print_function
import re

doc = __revit__.ActiveUIDocument.Document

#zona afetada pelo script
ZONA = "teste"

#tabelas dos isolamentos quentes - dmin, dmax, 40 a 65º, 66 a 100º  
TQ = [ [0, 25, 19, 19],
		[25, 50, 19, 30],
		[50, 76, 30, 30],
		[76, 127, 30, 40]]

#tabelas dos isolamentos quentes - dmin, dmax, 0,1 a 10º , > 10º
TF = [ [0, 25, 19, 19],
		[25, 50, 30, 19],
		[50, 76, 30, 30],
		[76, 127, 40, 30]]

#funcao de pesquisa nas tabelas dos isolamentos
def pesquisa(sistema, dn):
	if "Fria" in sistema:
		tabela = TF
	elif "Quente" in sistema:
		tabela = TQ
	else:
		return "Sem sistema"
		
	for linha in tabela:
			if dn <= linha[1]:
				espessura = linha[2]
				break
	return espessura

#funcao que altera as espessuras
def altera_espessuras(insulations, element):
	t = Transaction(doc,"Alteradas espessuras")
	t.Start()
	try:
		for i in range(len(insulations)):
			sistema = pipe.LookupParameter("System Type").AsValueString()
			dn = element.LookupParameter("Size").AsString()
			dn_i= int(re.search('(\d{2,})', dn).group(1))
			esp_atual = doc.GetElement(insulations[i]).Thickness
			doc.GetElement(insulations[i]).Thickness = float(pesquisa(sistema, dn_i) / 304.8)
						
			print(sistema + " com " + dn + " e com a actual espessura de " + str(esp_atual * 304.8) + " alterada para " + str(pesquisa(sistema, dn_i)))
			t.Commit()
	except:
		t.RollBack()


#coleciona todas as tubagens do modelo
pipes = FilteredElementCollector(doc) \
				.OfClass(Pipe) \
				.ToElements()	
				
#coleciona todas as fittings do modelo
fittings = FilteredElementCollector(doc) \
				.OfCategory(BuiltInCategory.OST_PipeFitting) \
				.WhereElementIsNotElementType() \
				.ToElements()
#pipes
for pipe in pipes:
	if pipe.LookupParameter("06.04.Zona").AsString() == ZONA:
		if pipe.GetType() != "Cobre ACR" and "PVC":
			insulations = InsulationLiningBase.GetInsulationIds(doc,pipe.Id)
			altera_espessuras(insulations, pipe)
				
#pipe fittings
for fitting in fittings:
	if fitting.LookupParameter("06.04.Zona").AsString() == ZONA:
		insulations = InsulationLiningBase.GetInsulationIds(doc,fitting.Id)
		altera_espessuras(insulations, fitting)