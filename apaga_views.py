sheets = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Sheets)
views = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Views)

lvs = []
for s in sheets:
	a = s.GetAllPlacedViews()
	for i in a:
		lvs.append(i)

lv = []
for v in views:
	lv.append(v.Id)

t = Transaction(doc, "Apagadas views")
t.Start()

try:
	for l in lv:
		view_nome = doc.GetElement(l).Name
		e_template = doc.GetElement(l).IsTemplate
		if l not in lvs and "working" not in view_nome and len(view_nome) > 3 and not e_template:
			print view_nome
			doc.Delete(l)
except:
	pass

t.Commit()
